object frmMain: TfrmMain
  Left = 0
  Top = 0
  ActiveControl = edtInput
  Caption = 'frmMain'
  ClientHeight = 256
  ClientWidth = 355
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object edtInput: TEdit
    Left = 8
    Top = 10
    Width = 121
    Height = 21
    TabOrder = 0
  end
  object btnLog: TButton
    Left = 144
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Add to log'
    TabOrder = 1
    OnClick = btnLogClick
  end
  object ListBox1: TListBox
    Left = 8
    Top = 48
    Width = 339
    Height = 200
    ItemHeight = 13
    TabOrder = 2
  end
end
