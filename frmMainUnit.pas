unit frmMainUnit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,
  LogUnit;

type
  TfrmMain = class(TForm)
    edtInput: TEdit;
    btnLog: TButton;
    ListBox1: TListBox;
    procedure FormCreate(Sender: TObject);
    procedure btnLogClick(Sender: TObject);
  private
    FLog : ILog;
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.dfm}

procedure TfrmMain.btnLogClick(Sender: TObject);
begin
  FLog.Log(edtInput.Text);
  FLog.Dump(ListBox1.Items);
end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  // NB: the creation order for the decorators is important
  //     the Log() method is called from the most recently
  //     created to the oldest. so TLogUser is first called and
  //     TLogToFile is last called (so it picks up all prior modifiecations)
  FLog := TLog.Create;
  FLog := TLogToFile.Create(FLog, ExtractFilePath(application.ExeName) + 'testlog.txt');
  FLog := TLogDateTime.Create(FLog);
  FLog := TLogUser.Create(FLog);
end;

end.
