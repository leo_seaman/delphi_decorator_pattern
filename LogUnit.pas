unit LogUnit;

interface

uses classes, sysutils, ioUtils, windows;

type
  ILog = interface
    procedure Log(Value : string);
    procedure Dump(Value : TStrings);
  end;

  TLog = class(TInterfacedObject, ILog)
  private
    FLog : TStringList;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Log(Value : string); virtual;
    procedure Dump(Value : TStrings); virtual;
  end;

  TLogDecorator = class(TLog)
  private
    FComponent : ILog;
  public
    constructor Create(ALog : ILog);
    procedure Log(Value : string); override;
    procedure Dump(Value : TStrings); override;
  end;

  TLogDateTime = class(TLogDecorator)
  private
    function AddTimeStamp(Value : string) : string;
  public
    procedure Log(Value: string); override;
  end;

  TLogUser = class(TLogDecorator)
  private
    function AddUserName(Value : string) : string;
    function WindowsUserName: String;
  public
    procedure Log(Value : string); override;
  end;

  TLogToFile = class(TLogDecorator)
  private
    FFileName : string;
    procedure AppendToFile(Value : string);
  public
    constructor Create(ALog : ILog; AFileName : string);
    procedure Log(Value : string); override;
  end;

implementation

{ TLog }


constructor TLog.Create;
begin
  FLog := TStringList.Create;
end;

destructor TLog.Destroy;
begin
  FLog.Free;
  inherited;
end;

procedure TLog.Dump(Value: TStrings);
begin
  Value.Assign(FLog);
end;

procedure TLog.Log(Value: string);
begin
  FLog.Add(Value);
end;

{ TLogDecorator }

constructor TLogDecorator.Create(ALog: ILog);
begin
  inherited Create;
  FComponent := ALog;
end;

procedure TLogDecorator.Dump(Value: TStrings);
begin
  FComponent.Dump(value);
end;

procedure TLogDecorator.Log(Value: string);
begin
  FComponent.Log(Value);
end;

{ TLogDateTime }

function TLogDateTime.AddTimeStamp(Value: string): string;
begin
  result := DateTimeToStr(now) + ', ' + Value;
end;

procedure TLogDateTime.Log(Value: string);
begin
  Value := AddTimeStamp(Value);   // modify behavior
  inherited Log(Value);           // continue calling other objects in the chain
end;

{ TLogUser }

function TLogUser.AddUserName(Value: string): string;
begin
  result := WindowsUserName + ', ' + value;
end;

procedure TLogUser.Log(Value: string);
begin
  value := AddUserName(value);
  inherited Log(value);
end;

function TLogUser.WindowsUserName: String;
var
  nSize: DWord;
begin
  nSize := 1024;
  SetLength(Result, nSize);
  if GetUserName(PChar(Result), nSize) then
    SetLength(Result, nSize-1)
  else
    RaiseLastOSError;
end;

{ TLogToFile }

procedure TLogToFile.AppendToFile(Value: string);
begin
  TFile.AppendAllText(FFileName, Value + sLineBreak);
end;

constructor TLogToFile.Create(ALog: ILog; AFileName : string);
begin
  inherited Create(ALog);
  FFileName := AFileName;
end;

procedure TLogToFile.Log(Value: string);
begin
  inherited;
  AppendToFile(Value);
end;

end.
