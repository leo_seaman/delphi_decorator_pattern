unit AccountUnit;

interface

uses
  System.Generics.Collections;

type
  TAccount = class
  private
    FBalance: currency;
    FAccountName: string;
  public
    constructor Create(AAccountName : string);
    property Balance : currency read FBalance;
    property AccountName : string read FAccountName write FAccountName;
    procedure Add(Amt : currency);
  end;

  TAccountList = TObjectList<TAccount>;

implementation

{ TAccount }

procedure TAccount.Add(Amt: currency);
begin
  FBalance := FBalance + Amt;
end;

constructor TAccount.Create(AAccountName: string);
begin
  inherited Create;
  if AAccountName = '' then
    AAccountName := 'Undefined Account';

  FAccountName := AAccountName;
end;

end.
